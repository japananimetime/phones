@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey())
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="/phones/blog/public/index.php/upload"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" name="excel" id="upload">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select name="column" id="columnPhone"></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-dark" type="submit"> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>

    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                };

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        var ExcelToJSON = function () {

            this.parseExcel = function (file) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, {
                        type: 'binary'
                    });
                    $headersNames = get_header_row(workbook.Sheets[workbook.SheetNames[0]]);
                    $headersNames.forEach(function (element) {
                        var temp = document.createElement('option');
                        var tempText = document.createTextNode(element);
                        temp.value = element;
                        temp.appendChild(tempText);
                        document.getElementById('columnPhone').appendChild(temp);
                    })
                };

                reader.onerror = function (ex) {
                    console.log(ex);
                };

                reader.readAsBinaryString(file);
            };
        };

        function handleFileSelect(evt) {

            var files = evt.target.files; // FileList object
            var xl2json = new ExcelToJSON();
            xl2json.parseExcel(files[0]);
        }

        function get_header_row(sheet) {
            var headers = [];
            var range = XLSX.utils.decode_range(sheet['!ref']);
            var C, R = range.s.r; /* start in the first row */
            /* walk every column in the range */
            for (C = range.s.c; C <= range.e.c; ++C) {
                var cell = sheet[XLSX.utils.encode_cell({c: C, r: R})]; /* find the cell in the first row */

                var hdr = "UNKNOWN " + C; // <-- replace with your desired default
                if (cell && cell.t) hdr = XLSX.utils.format_cell(cell);

                headers.push(hdr);
            }
            return headers;
        }

        document.getElementById('upload').addEventListener('change', handleFileSelect, false);

        !function (a, b) {
            if ("object" == typeof exports && "object" == typeof module) module.exports = b(require("jquery")); else if ("function" == typeof define && define.amd) define(["jquery"], b); else {
                var c = b("object" == typeof exports ? require("jquery") : a.jQuery);
                for (var d in c) ("object" == typeof exports ? exports : a)[d] = c[d]
            }
        }(this, function (a) {
            return function (a) {
                function b(d) {
                    if (c[d]) return c[d].exports;
                    var e = c[d] = {exports: {}, id: d, loaded: !1};
                    return a[d].call(e.exports, e, e.exports, b), e.loaded = !0, e.exports
                }

                var c = {};
                return b.m = a, b.c = c, b.p = "", b(0).default
            }([function (a, b, c) {
                "use strict";
                Object.defineProperty(b, "__esModule", {value: !0});
                var d = c(1), e = function (a) {
                    return a && a.__esModule ? a : {default: a}
                }(d), f = e.default, g = {append: null}, h = function (a, b) {
                    var c = this;
                    c.options = f.extend({}, g, b);
                    var d = 0, e = 0, h = c.$el = f(a), i = b.append ? f(b.append) : null;
                    c.$el.on("remove", function () {
                        c.destroy()
                    });
                    var j = function (a) {
                        ("function" != typeof b.allowDrop || b.allowDrop(a.originalEvent.dataTransfer)) && a.preventDefault()
                    }, k = function (a) {
                        if ("function" == typeof b.allowDrop && !b.allowDrop(a.originalEvent.dataTransfer)) return void a.preventDefault();
                        void 0 !== a && 1 != ++e || (h.addClass("drop-over"), i && i.addClass("drop-over"))
                    }, l = function () {
                        if ("undefined" != typeof event) {
                            if ((e = Math.max(e - 1, 0)) > 0) return
                        } else e = 0;
                        h.removeClass("drop-over"), i && i.removeClass("drop-over")
                    }, m = function () {
                        p()
                    }, n = function (a) {
                        if ("function" == typeof b.allowDrop && !b.allowDrop(a.originalEvent.dataTransfer)) return void a.preventDefault();
                        void 0 !== a && 1 != ++d || (h.addClass("drop-available"), i && i.appendTo(h), (i || h).on("dragover", j).on("dragenter", k).on("dragleave", l).on("drop", m))
                    }, o = function (a) {
                        ("function" != typeof b.allowDrop || b.allowDrop(a.originalEvent.dataTransfer)) && a.preventDefault()
                    }, p = function (a) {
                        if (void 0 !== a) {
                            if ((d = Math.max(d - 1, 0)) > 0) return
                        } else d = 0;
                        h.removeClass("drop-available"), i && i.detach(), (i || h).off("dragover", j).off("dragenter", k).off("dragleave", l).off("drop", m), l()
                    }, q = function (a) {
                        setTimeout(function () {
                            p()
                        }, 0)
                    };
                    f(window).on("dragover", o).on("dragenter", n).on("dragleave", p).on("drop", q), h.data("global-dropzone", {
                        allowGlobalDrag: o,
                        startGlobalDrag: n,
                        endGlobalDrag: p,
                        globalDrop: q
                    })
                };
                h.prototype.destroy = function () {
                    if (this.$el) {
                        var a = this.$el, b = a.data("global-dropzone");
                        b && (f(window).off("dragover", b.allowGlobalDrag).off("dragenter", b.startGlobalDrag).off("dragleave", b.endGlobalDrag).off("drop", b.globalDrop), b.endGlobalDrag(), a.removeData("global-dropzone")), a.data("dropzone") == this && a.removeData("dropzone"), this.options = null
                    }
                }, f.fn.dropzone = function (a) {
                    var b, c = arguments;
                    return this.each(function (d) {
                        var e = f(this), g = e.data("dropzone");
                        if ("string" != typeof c[0]) {
                            if (g) try {
                                g.destroy()
                            } catch (a) {
                            }
                            a = f.extend({}, a || {}), g = new h(this, a), e.data("dropzone", g)
                        } else if (g && "function" == typeof g[c[0]]) {
                            var i = g[c[0]].apply(g, Array.prototype.slice.call(c, 1));
                            return i === g && (i = void 0), void (void 0 !== i && (b = b || [], b[d] = i))
                        }
                    }), b ? 1 === b.length ? b[0] : b : this
                }, b.default = h
            }, function (b, c) {
                b.exports = a
            }])
        });

        $('#upload').dropzone()
    </script>
@stop
