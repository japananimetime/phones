-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: phones
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2020-03-07 18:34:44','2020-03-07 18:34:44'),(2,NULL,1,'Category 2','category-2','2020-03-07 18:34:44','2020-03-07 18:34:44');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Supplygroup','2020-03-09 09:45:59','2020-03-09 09:45:59'),(2,'Spark Logistics','2020-03-09 12:35:39','2020-03-09 12:35:39');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'{}',1),(2,1,'name','text','Имя',1,1,1,1,1,1,'{}',2),(3,1,'email','text','Email',1,1,1,1,1,1,'{}',4),(4,1,'password','password','Пароль',1,0,0,1,1,0,'{}',5),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'{}',6),(6,1,'created_at','timestamp','Создана в',0,1,1,0,0,0,'{}',7),(7,1,'updated_at','timestamp','Обновлена в',0,0,0,0,0,0,'{}',9),(8,1,'avatar','image','Avatar',0,0,0,0,0,0,'{}',10),(9,1,'user_belongsto_role_relationship','relationship','Роль',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',12),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,0,0,0,0,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',13),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,'{}',15),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,'{}',1),(17,3,'name','text','Name',1,1,1,1,1,1,'{}',2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,'{}',5),(21,1,'role_id','text','Role',0,1,1,1,1,1,'{}',11),(56,1,'email_verified_at','timestamp','Email Verified At',0,0,0,0,0,0,'{}',8),(57,1,'company_id','text','Company Id',1,1,1,1,1,1,'{}',14),(58,1,'user_belongsto_company_relationship','relationship','Компания',0,1,1,1,1,1,'{\"model\":\"App\\\\Company\",\"table\":\"companies\",\"type\":\"belongsTo\",\"column\":\"company_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(59,7,'id','text','Номер',1,0,0,0,0,0,'{}',1),(60,7,'name','text','Название',1,1,1,1,1,1,'{}',2),(61,7,'created_at','timestamp','Создана в',0,1,1,1,0,1,'{}',3),(62,7,'updated_at','timestamp','Обновлена в',0,0,0,0,0,0,'{}',4),(63,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(64,8,'file_id','text','Номер файла',1,1,1,1,1,1,'{}',3),(65,8,'name','text','Колонка',1,1,1,1,1,1,'{}',4),(66,8,'value','text','Значение',1,1,1,1,1,1,'{}',5),(67,8,'created_at','timestamp','Создана в',0,1,1,1,0,1,'{}',6),(68,8,'updated_at','timestamp','Обновлена в',0,0,0,0,0,0,'{}',7),(69,8,'result_belongsto_file_relationship','relationship','files',0,1,1,1,1,1,'{\"model\":\"App\\\\File\",\"table\":\"files\",\"type\":\"belongsTo\",\"column\":\"file_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(70,9,'id','text','Id',1,0,0,0,0,0,'{}',1),(71,9,'user_id','text','Пользователь',1,1,1,1,1,1,'{}',4),(72,9,'company_id','text','Компания',1,1,1,1,1,1,'{}',5),(73,9,'name','text','Название',1,1,1,1,1,1,'{}',6),(74,9,'link','text','Ссылка',1,0,1,1,1,1,'{}',7),(75,9,'created_at','timestamp','Создана в',0,0,1,1,0,1,'{}',8),(76,9,'updated_at','timestamp','Обновлена в',0,0,0,0,0,0,'{}',9),(77,10,'id','text','Id',1,0,0,0,0,0,'{}',1),(78,10,'phone','text','Телефон',0,1,1,1,1,1,'{}',2),(79,10,'created_at','timestamp','Создана в',0,1,1,1,0,1,'{}',3),(80,10,'updated_at','timestamp','Обновлена в',0,0,0,0,0,0,'{}',4),(81,10,'ls','text','Ls',0,1,1,1,1,1,'{}',5),(82,10,'company','text','Company',0,1,1,1,1,1,'{}',6),(83,9,'file_belongsto_company_relationship','relationship','Компания',0,1,1,1,1,1,'{\"model\":\"App\\\\Company\",\"table\":\"companies\",\"type\":\"belongsTo\",\"column\":\"company_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(84,9,'file_belongsto_user_relationship','relationship','Пользователь',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(85,10,'company_id','text','Company Id',1,0,0,0,0,0,'{}',7);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','App\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-03-07 18:34:44','2020-03-09 12:15:26'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-03-07 18:34:44','2020-03-09 09:51:36'),(7,'companies','companies','Company','Companies',NULL,'App\\Company',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2020-03-09 05:00:22','2020-03-09 05:00:22'),(8,'results','results','Result','Results',NULL,'App\\Result',NULL,'App\\Http\\Controllers\\ResultController',NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-03-09 05:02:28','2020-03-09 05:26:35'),(9,'files','files','File','Files',NULL,'App\\File',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-03-09 05:04:35','2020-03-09 11:31:23'),(10,'phones','phones','Phone','Phones',NULL,'App\\Phone',NULL,'App\\Http\\Controllers\\PhoneController',NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-03-09 05:09:09','2020-03-09 12:35:04');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (8,'1','1','тел номера_ВСЕ.xlsx','http://127.0.0.1:8000/storage/users/1/files/phones/u5tnczQFAgCCQkWfVWMIotOeCNMjxcvxbZ9QjdXe.xlsx','2020-03-09 12:33:51','2020-03-09 12:33:51');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,4,'2020-03-07 18:34:44','2020-03-07 19:23:41','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2020-03-07 18:34:44','2020-03-07 18:34:44',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.settings.index',NULL),(14,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2020-03-07 18:34:44','2020-03-07 18:34:44','voyager.hooks',NULL),(15,1,'Companies','','_self',NULL,NULL,NULL,15,'2020-03-09 05:00:22','2020-03-09 05:00:22','voyager.companies.index',NULL),(16,1,'Results','','_self',NULL,NULL,NULL,16,'2020-03-09 05:02:28','2020-03-09 05:02:28','voyager.results.index',NULL),(17,1,'Files','','_self',NULL,NULL,NULL,17,'2020-03-09 05:04:35','2020-03-09 05:04:35','voyager.files.index',NULL),(18,1,'Phones','','_self',NULL,NULL,NULL,18,'2020-03-09 05:09:09','2020-03-09 05:09:09','voyager.phones.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2020-03-07 18:34:44','2020-03-07 18:34:44');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1),(24,'2016_01_01_000000_create_pages_table',2),(25,'2016_01_01_000000_create_posts_table',2),(26,'2016_02_15_204651_create_categories_table',2),(27,'2017_04_11_000000_alter_post_nullable_fields_table',2),(28,'2020_03_07_192503_create_companies_table',3),(29,'2020_03_07_192752_create_files_table',3),(30,'2020_03_07_192808_create_results_table',3),(31,'2020_03_08_000000_alter_users_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2020-03-07 18:34:44','2020-03-07 18:34:44');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(1,3),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(47,2),(47,3),(48,1),(49,1),(50,1),(51,1),(52,1),(52,2),(52,3),(53,1),(54,1),(55,1),(55,2),(56,1),(57,1),(57,2),(57,3),(58,1),(59,1),(60,1),(60,3),(61,1),(61,3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(2,'browse_bread',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(3,'browse_database',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(4,'browse_media',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(5,'browse_compass',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(6,'browse_menus','menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(7,'read_menus','menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(8,'edit_menus','menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(9,'add_menus','menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(10,'delete_menus','menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(11,'browse_roles','roles','2020-03-07 18:34:44','2020-03-07 18:34:44'),(12,'read_roles','roles','2020-03-07 18:34:44','2020-03-07 18:34:44'),(13,'edit_roles','roles','2020-03-07 18:34:44','2020-03-07 18:34:44'),(14,'add_roles','roles','2020-03-07 18:34:44','2020-03-07 18:34:44'),(15,'delete_roles','roles','2020-03-07 18:34:44','2020-03-07 18:34:44'),(16,'browse_users','users','2020-03-07 18:34:44','2020-03-07 18:34:44'),(17,'read_users','users','2020-03-07 18:34:44','2020-03-07 18:34:44'),(18,'edit_users','users','2020-03-07 18:34:44','2020-03-07 18:34:44'),(19,'add_users','users','2020-03-07 18:34:44','2020-03-07 18:34:44'),(20,'delete_users','users','2020-03-07 18:34:44','2020-03-07 18:34:44'),(21,'browse_settings','settings','2020-03-07 18:34:44','2020-03-07 18:34:44'),(22,'read_settings','settings','2020-03-07 18:34:44','2020-03-07 18:34:44'),(23,'edit_settings','settings','2020-03-07 18:34:44','2020-03-07 18:34:44'),(24,'add_settings','settings','2020-03-07 18:34:44','2020-03-07 18:34:44'),(25,'delete_settings','settings','2020-03-07 18:34:44','2020-03-07 18:34:44'),(41,'browse_hooks',NULL,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(42,'browse_companies','companies','2020-03-09 05:00:22','2020-03-09 05:00:22'),(43,'read_companies','companies','2020-03-09 05:00:22','2020-03-09 05:00:22'),(44,'edit_companies','companies','2020-03-09 05:00:22','2020-03-09 05:00:22'),(45,'add_companies','companies','2020-03-09 05:00:22','2020-03-09 05:00:22'),(46,'delete_companies','companies','2020-03-09 05:00:22','2020-03-09 05:00:22'),(47,'browse_results','results','2020-03-09 05:02:28','2020-03-09 05:02:28'),(48,'read_results','results','2020-03-09 05:02:28','2020-03-09 05:02:28'),(49,'edit_results','results','2020-03-09 05:02:28','2020-03-09 05:02:28'),(50,'add_results','results','2020-03-09 05:02:28','2020-03-09 05:02:28'),(51,'delete_results','results','2020-03-09 05:02:28','2020-03-09 05:02:28'),(52,'browse_files','files','2020-03-09 05:04:35','2020-03-09 05:04:35'),(53,'read_files','files','2020-03-09 05:04:35','2020-03-09 05:04:35'),(54,'edit_files','files','2020-03-09 05:04:35','2020-03-09 05:04:35'),(55,'add_files','files','2020-03-09 05:04:35','2020-03-09 05:04:35'),(56,'delete_files','files','2020-03-09 05:04:35','2020-03-09 05:04:35'),(57,'browse_phones','phones','2020-03-09 05:09:09','2020-03-09 05:09:09'),(58,'read_phones','phones','2020-03-09 05:09:09','2020-03-09 05:09:09'),(59,'edit_phones','phones','2020-03-09 05:09:09','2020-03-09 05:09:09'),(60,'add_phones','phones','2020-03-09 05:09:09','2020-03-09 05:09:09'),(61,'delete_phones','phones','2020-03-09 05:09:09','2020-03-09 05:09:09');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phones`
--

DROP TABLE IF EXISTS `phones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ls` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=699 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phones`
--

LOCK TABLES `phones` WRITE;
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
INSERT INTO `phones` VALUES (1,'2800660','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(2,'3311000','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(3,'79997','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(4,'235233','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(5,'235234','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(6,'2581285','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(7,'2581712','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(8,'2588810','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(9,'3300799','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(10,'3307029','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(11,'3307057','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(12,'3307502','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(13,'3307550','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(14,'3307565','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(15,'3307577','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(16,'3309990','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(17,'3310300','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(18,'3311124','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(19,'3311352','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(20,'3316395','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(21,'554450','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(22,'554458','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(23,'554459','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(24,'554460','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(25,'554516','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(26,'704251','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(27,'79917','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(28,'79937','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(29,'79938','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(30,'79990','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(31,'79992','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(32,'79993','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(33,'79994','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(34,'79998','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(35,'990174','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(36,'990175','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(37,'2588814','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(38,'3307397','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(39,'3307567','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(40,'3307568','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(41,'3311123','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(42,'3311125','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(43,'3316313','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(44,'3326677','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(45,'3331974','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(46,'552181','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(47,'554454','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(48,'554457','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(49,'559245','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(50,'559308','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(51,'290514','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(52,'290530','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(53,'3307026','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(54,'3313663','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(55,'3316312','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(56,'3316565','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(57,'3316566','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(58,'3317707','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(59,'3319000','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(60,'3319009','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(61,'66','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(62,'552182','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(63,'554515','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(64,'999840','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(65,'2444666','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(66,'2445312','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(67,'2445328','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(68,'2445329','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(69,'2581287','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(70,'2581288','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(71,'2581713','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(72,'2581738','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(73,'2582512','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(74,'2582513','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(75,'2584912','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(76,'2588240','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(77,'2588811','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(78,'2588812','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(79,'2588813','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(80,'2588815','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(81,'2596260','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(82,'2597560','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(83,'2597859','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(84,'2981250','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(85,'3300957','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(86,'3303996','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(87,'3303997','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(88,'3303998','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(89,'3303999','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(90,'3306688','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(91,'3307036','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(92,'3307095','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(93,'3307096','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(94,'3307097','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(95,'3307279','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(96,'3307291','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(97,'3307295','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(98,'3307296','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(99,'3307297','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(100,'3307307','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(101,'3307322','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(102,'3307323','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(103,'3307385','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(104,'3307398','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(105,'3307399','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(106,'3307400','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(107,'3307401','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(108,'3307403','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(109,'3307404','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(110,'3307506','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(111,'3307507','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(112,'3307515','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(113,'3307520','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(114,'3307551','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(115,'3307564','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(116,'3307566','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(117,'3307912','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(118,'3307913','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(119,'3308111','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(120,'3308192','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(121,'3308193','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(122,'3308194','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(123,'3308197','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(124,'3309493','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(125,'3309494','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(126,'3309495','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(127,'3309994','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(128,'3310077','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(129,'3310777','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(130,'3311001','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(131,'3311002','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(132,'3311003','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(133,'3311004','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(134,'3311005','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(135,'3311006','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(136,'3311007','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(137,'3311008','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(138,'3311009','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(139,'3311011','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(140,'3311012','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(141,'3311013','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(142,'3311233','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(143,'3311278','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(144,'3311311','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(145,'3311351','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(146,'3311414','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(147,'3311500','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(148,'3313663','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(149,'3313664','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(150,'3314748','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(151,'3314749','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(152,'3314777','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(153,'3314826','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(154,'3314834','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(155,'3314857','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(156,'3314866','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(157,'3314867','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(158,'3314940','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(159,'3314941','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(160,'3314942','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(161,'3314943','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(162,'3314946','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(163,'3314974','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(164,'3315002','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(165,'3315003','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(166,'3315039','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(167,'3315040','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(168,'3315454','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(169,'3316333','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(170,'3316394','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(171,'3316580','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(172,'3316581','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(173,'3316582','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(174,'3316583','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(175,'3316584','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(176,'3316585','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(177,'3316586','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(178,'3316587','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(179,'3316588','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(180,'3316589','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(181,'3316633','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(182,'3316693','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(183,'3316694','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(184,'3316697','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(185,'3316698','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(186,'3318501','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(187,'3318502','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(188,'3318503','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(189,'3318504','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(190,'3318883','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(191,'3318884','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(192,'3318885','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(193,'3325577','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(194,'3331535','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(195,'3331545','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(196,'3331975','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(197,'3903923','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(198,'3912095','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(199,'3930502','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(200,'45044','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(201,'552181','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(202,'554244','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(203,'554451','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(204,'554452','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(205,'554453','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(206,'554456','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(207,'554461','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(208,'554462','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(209,'554544','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(210,'554820','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(211,'558037','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(212,'558039','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(213,'558446','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(214,'558748','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(215,'559245','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(216,'559899','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(217,'597859','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(218,'701210','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(219,'701211','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(220,'70206_А','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(221,'703864','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(222,'703872','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(223,'703874','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(224,'703956','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(225,'703968','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(226,'703969','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(227,'704252','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(228,'704365','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(229,'709616','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(230,'79923','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(231,'79933','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(232,'79934','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(233,'79936','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(234,'79939','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(235,'79940','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(236,'79949','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(237,'79991','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(238,'79999','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(239,'997623','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(240,'999732','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(241,'290515','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(242,'401082','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(243,'401084','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(244,'3331527','2020-03-09 12:33:51','2020-03-09 12:33:51','70206','AsTi',1),(245,'570104','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(246,'570103','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(247,'518273','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(248,'518054','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(249,'476100','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(250,'266922','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(251,'266911','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(252,'794607640','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi г. Алматы, ул. ДЖАНДОСОВА, д.162/а',1),(253,'383284','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(254,'360093','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(255,'360092','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(256,'3307260','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(257,'3307384','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(258,'3307185','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(259,'510067479','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi  бизнес-транк',1),(260,'3307260','2020-03-09 12:33:51','2020-03-09 12:33:51','80808','AsTi',1),(261,'1707','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(262,'18555','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(263,'194310957','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(264,'194328437','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(265,'201417','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(266,'203653968','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(267,'221051','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(268,'221244','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(269,'221817','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(270,'222038','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(271,'222570','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(272,'223815','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(273,'223910','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(274,'227233','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(275,'23297','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(276,'2440200','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(277,'2440201','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(278,'2440202','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(279,'2440203','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(280,'2440204','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(281,'2440205','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(282,'2440206','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(283,'2440207','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(284,'2440208','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(285,'2440209','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(286,'2442806','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(287,'2442809','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(288,'2445044','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(289,'2445050','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(290,'2445544','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(291,'2446020','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(292,'2446367','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(293,'2446368','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(294,'2446375','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(295,'2446376','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(296,'2448768','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(297,'2448769','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(298,'2448770','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(299,'2448771','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(300,'2448772','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(301,'2448773','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(302,'2448774','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(303,'2448775','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(304,'2448776','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(305,'2448777','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(306,'2448778','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(307,'2448779','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(308,'2448780','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(309,'2448781','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(310,'2448782','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(311,'2448783','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(312,'2448784','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(313,'2448785','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(314,'2448786','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(315,'2448787','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(316,'2448788','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(317,'2448789','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(318,'2448790','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(319,'2448791','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(320,'2448792','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(321,'2448793','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(322,'2448794','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(323,'2448795','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(324,'2448796','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(325,'2448797','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(326,'2448798','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(327,'2448799','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(328,'2448800','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(329,'2448801','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(330,'2448802','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(331,'2448803','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(332,'2448804','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(333,'2448805','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(334,'2448806','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(335,'2448807','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(336,'2448808','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(337,'2448809','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(338,'2448810','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(339,'2448811','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(340,'2448812','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(341,'2448813','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(342,'2448814','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(343,'2581058','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(344,'2581060','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(345,'2581085','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(346,'2581262','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(347,'2581573','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(348,'2581787','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(349,'2581920','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(350,'2581982','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(351,'2582020','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(352,'2582036','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(353,'2582037','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(354,'2582038','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(355,'2582039','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(356,'2582331','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(357,'2583429','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(358,'2583430','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(359,'2583431','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(360,'2583432','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(361,'2583433','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(362,'2583434','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(363,'2583435','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(364,'2583436','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(365,'2583437','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(366,'2583438','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(367,'2583439','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(368,'2583440','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(369,'2583441','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(370,'2583442','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(371,'2583443','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(372,'2583444','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(373,'2583445','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(374,'2583446','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(375,'2583447','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(376,'2583448','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(377,'2583449','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(378,'2583450','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(379,'2583456','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(380,'2583505','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(381,'2584477','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(382,'2584550','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(383,'2584551','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(384,'2584700','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(385,'2584885','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(386,'2584951','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(387,'2584952','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(388,'2584953','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(389,'2584954','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(390,'2585001','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(391,'2585002','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(392,'2585003','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(393,'2585004','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(394,'2585005','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(395,'2585018','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(396,'2585019','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(397,'2585520','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(398,'2585521','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(399,'2585522','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(400,'2585524','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(401,'2585525','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(402,'2585526','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(403,'2585527','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(404,'2585529','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(405,'2587030','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(406,'2587695','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(407,'2587988','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(408,'2588985','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(409,'2590696','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(410,'2596868','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(411,'2599200','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(412,'2599201','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(413,'2599202','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(414,'2599203','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(415,'2599206','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(416,'2599212','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(417,'2599214','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(418,'2599218','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(419,'2599221','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(420,'2599222','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(421,'2599225','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(422,'2599226','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(423,'2599235','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(424,'2599236','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(425,'2599241','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(426,'2599247','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(427,'2599250','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(428,'2599255','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(429,'2599260','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(430,'2599262','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(431,'2599266','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(432,'2599276','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(433,'2599279','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(434,'2599284','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(435,'2599287','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(436,'2599288','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(437,'2599292','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(438,'2599293','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(439,'2599295','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(440,'2599296','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(441,'2599297','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(442,'2599298','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(443,'310415','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(444,'310416','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(445,'310417','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(446,'3300122','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(447,'3300127','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(448,'3300135','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(449,'3300138','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(450,'3300142','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(451,'3300145','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(452,'3300146','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(453,'3300147','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(454,'3300148','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(455,'3300402','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(456,'3300416','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(457,'3300417','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(458,'3300424','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(459,'3300425','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(460,'3300427','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(461,'3300436','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(462,'3300439','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(463,'3300440','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(464,'3300441','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(465,'3300443','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(466,'3300449','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(467,'3300561','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(468,'3300564','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(469,'3300567','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(470,'3300571','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(471,'3300573','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(472,'3300575','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(473,'3300582','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(474,'3300585','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(475,'3300587','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(476,'3300588','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(477,'3300591','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(478,'3300593','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(479,'3300594','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(480,'3300596','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(481,'3300597','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(482,'3300599','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(483,'3300604','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(484,'3300605','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(485,'3300608','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(486,'3300609','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(487,'3300612','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(488,'3300613','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(489,'3300614','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(490,'3300616','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(491,'3300617','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(492,'3300622','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(493,'3300631','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(494,'3300634','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(495,'3300637','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(496,'3300639','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(497,'3300641','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(498,'3300644','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(499,'3300646','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(500,'3300649','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(501,'3300650','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(502,'3300651','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(503,'3300652','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(504,'3300653','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(505,'3300655','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(506,'3300661','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(507,'3300663','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(508,'3300667','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(509,'3300674','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(510,'3300677','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(511,'3300679','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(512,'3300680','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(513,'3300683','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(514,'3300687','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(515,'3300688','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(516,'3300689','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(517,'3300692','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(518,'3300696','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(519,'3300698','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(520,'3300699','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(521,'3300701','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(522,'3300709','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(523,'3300711','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(524,'3300712','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(525,'3300722','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(526,'3300728','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(527,'3300746','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(528,'3300749','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(529,'3300757','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(530,'3300763','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(531,'3300764','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(532,'3300768','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(533,'3300774','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(534,'3300775','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(535,'3300776','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(536,'3300778','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(537,'3300779','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(538,'3300783','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(539,'3302000','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(540,'3302006','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(541,'3302070','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(542,'3302075','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(543,'3302076','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(544,'3302080','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(545,'3302083','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(546,'3302084','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(547,'3302085','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(548,'3302086','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(549,'3302087','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(550,'3302088','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(551,'3302090','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(552,'3302242','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(553,'3302246','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(554,'3302251','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(555,'3302260','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(556,'3302261','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(557,'3302262','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(558,'3302263','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(559,'3302266','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(560,'3302267','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(561,'3302269','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(562,'3302271','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(563,'3303995','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(564,'3321122','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(565,'3321133','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(566,'3321331','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(567,'3325555','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(568,'3325777','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(569,'3327755','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(570,'3327757','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(571,'3327758','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(572,'3327777','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(573,'3327788','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(574,'3327997','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(575,'3328851','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(576,'3328852','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(577,'3328853','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(578,'3328854','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(579,'3328855','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(580,'3328857','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(581,'3328877','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(582,'3328887','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(583,'3328888','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(584,'3328889','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(585,'3328890','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(586,'3328899','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(587,'358399','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(588,'448136','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(589,'479929340','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(590,'525216','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(591,'527737','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(592,'53837','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(593,'580219','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(594,'580252','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(595,'580253','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(596,'580254','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(597,'580255','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(598,'580455','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(599,'580459','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(600,'580876','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(601,'580985','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(602,'583434','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(603,'586150','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(604,'586151','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(605,'591941','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(606,'592765','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(607,'593252','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(608,'593253','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(609,'5959','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(610,'6462.2','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(611,'652','2020-03-09 12:33:51','2020-03-09 12:33:51','61903','AsTi',1),(612,'3308877','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(613,'3310004','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(614,'3310005','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(615,'3310006','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(616,'3310007','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(617,'3310008','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(618,'3310009','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(619,'3310010','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(620,'3310011','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(621,'3310012','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(622,'3310013','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(623,'3310014','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(624,'3310015','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(625,'3310016','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(626,'3310017','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(627,'3310018','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(628,'3310019','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(629,'3310020','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(630,'3310021','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(631,'3310022','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(632,'3310023','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(633,'3310024','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(634,'3310025','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(635,'3310026','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(636,'3310027','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(637,'3310028','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(638,'3310029','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(639,'3310030','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(640,'3310031','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(641,'3310032','2020-03-09 12:33:51','2020-03-09 12:33:51','111008','AsTi',1),(642,'3308428','2020-03-09 12:33:51','2020-03-09 12:33:51','18555','AsTi',1),(643,'2583428','2020-03-09 12:33:51','2020-03-09 12:33:51','1707','AsTi',1),(644,'2585523','2020-03-09 12:33:51','2020-03-09 12:33:51','1707','AsTi',1),(645,'2585528','2020-03-09 12:33:51','2020-03-09 12:33:51','1707','AsTi',1),(646,'1658872308','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Altenge',1),(647,'235689','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(648,'235690','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(649,'235835','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AlSolutions',1),(650,'235836','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AlSolutions',1),(651,'235838','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AlSolutions',1),(652,'2590044','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(653,'2590048','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(654,'2590049','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(655,'2590956','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(656,'2597777','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(657,'2599110','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(658,'2599115','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(659,'2599909','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Bell  tour',1),(660,'3310505','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Pediatri Alliance',1),(661,'3310506','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Pediatri Alliance',1),(662,'3311171','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Офис',1),(663,'3311180','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Рыбпром ',1),(664,'3311379','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Aqua Food Trade',1),(665,'3311382','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Aqua Food Trade',1),(666,'3313160','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Pediatri Alliance',1),(667,'3316434','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Офис',1),(668,'3316446','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Офис',1),(669,'3331527','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AsTi',1),(670,'3331588','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Altenge',1),(671,'3331775','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Azimut Soutions',1),(672,'450013','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AsTi',1),(673,'450028','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AsTi',1),(674,'552083','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(675,'554150','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Учебно-научный комплекс',1),(676,'554151','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Учебно-научный комплекс',1),(677,'554482','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Байман',1),(678,'554483','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Байман',1),(679,'554484','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Байман',1),(680,'554485','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Байман',1),(681,'554644','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(682,'554646','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(683,'554655','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(684,'554656','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(685,'554893','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(686,'559060','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(687,'559088','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(688,'559197','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(689,'559198','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','KZ Systems',1),(690,'559240','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','BCD Company',1),(691,'559241','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','BCD Company',1),(692,'560244','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AlSolutions',1),(693,'560245','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','AlSolutions',1),(694,'79922','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Офис 1459118',1),(695,'916419','2020-03-09 12:33:51','2020-03-09 12:33:51','1459118','Arman Kala',1),(696,NULL,'2020-03-09 12:33:51','2020-03-09 12:33:51',NULL,NULL,1),(697,NULL,'2020-03-09 12:33:51','2020-03-09 12:33:51',NULL,NULL,1),(698,NULL,'2020-03-09 12:33:51','2020-03-09 12:33:51',NULL,NULL,1);
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2020-03-07 18:34:44','2020-03-07 18:34:44'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2020-03-07 18:34:44','2020-03-07 18:34:44');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `results` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(20) unsigned NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Администратор','2020-03-07 18:34:44','2020-03-09 05:05:28'),(2,'user','Менеджер','2020-03-07 18:34:44','2020-03-09 05:06:39'),(3,'company','Директор','2020-03-09 05:07:45','2020-03-09 05:07:45');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2020-03-07 18:34:44','2020-03-07 18:34:44'),(2,'data_types','display_name_singular',6,'pt','Página','2020-03-07 18:34:44','2020-03-07 18:34:44'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2020-03-07 18:34:44','2020-03-07 18:34:44'),(4,'data_types','display_name_singular',4,'pt','Categoria','2020-03-07 18:34:44','2020-03-07 18:34:44'),(5,'data_types','display_name_singular',2,'pt','Menu','2020-03-07 18:34:44','2020-03-07 18:34:44'),(6,'data_types','display_name_singular',3,'pt','Função','2020-03-07 18:34:44','2020-03-07 18:34:44'),(7,'data_types','display_name_plural',5,'pt','Posts','2020-03-07 18:34:44','2020-03-07 18:34:44'),(8,'data_types','display_name_plural',6,'pt','Páginas','2020-03-07 18:34:44','2020-03-07 18:34:44'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2020-03-07 18:34:44','2020-03-07 18:34:44'),(10,'data_types','display_name_plural',4,'pt','Categorias','2020-03-07 18:34:44','2020-03-07 18:34:44'),(11,'data_types','display_name_plural',2,'pt','Menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(12,'data_types','display_name_plural',3,'pt','Funções','2020-03-07 18:34:44','2020-03-07 18:34:44'),(13,'categories','slug',1,'pt','categoria-1','2020-03-07 18:34:44','2020-03-07 18:34:44'),(14,'categories','name',1,'pt','Categoria 1','2020-03-07 18:34:44','2020-03-07 18:34:44'),(15,'categories','slug',2,'pt','categoria-2','2020-03-07 18:34:44','2020-03-07 18:34:44'),(16,'categories','name',2,'pt','Categoria 2','2020-03-07 18:34:44','2020-03-07 18:34:44'),(17,'pages','title',1,'pt','Olá Mundo','2020-03-07 18:34:44','2020-03-07 18:34:44'),(18,'pages','slug',1,'pt','ola-mundo','2020-03-07 18:34:44','2020-03-07 18:34:44'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2020-03-07 18:34:44','2020-03-07 18:34:44'),(20,'menu_items','title',1,'pt','Painel de Controle','2020-03-07 18:34:44','2020-03-07 18:34:44'),(21,'menu_items','title',2,'pt','Media','2020-03-07 18:34:44','2020-03-07 18:34:44'),(22,'menu_items','title',12,'pt','Publicações','2020-03-07 18:34:44','2020-03-07 18:34:44'),(23,'menu_items','title',3,'pt','Utilizadores','2020-03-07 18:34:44','2020-03-07 18:34:44'),(24,'menu_items','title',11,'pt','Categorias','2020-03-07 18:34:44','2020-03-07 18:34:44'),(25,'menu_items','title',13,'pt','Páginas','2020-03-07 18:34:44','2020-03-07 18:34:44'),(26,'menu_items','title',4,'pt','Funções','2020-03-07 18:34:44','2020-03-07 18:34:44'),(27,'menu_items','title',5,'pt','Ferramentas','2020-03-07 18:34:44','2020-03-07 18:34:44'),(28,'menu_items','title',6,'pt','Menus','2020-03-07 18:34:44','2020-03-07 18:34:44'),(29,'menu_items','title',7,'pt','Base de dados','2020-03-07 18:34:44','2020-03-07 18:34:44'),(30,'menu_items','title',10,'pt','Configurações','2020-03-07 18:34:44','2020-03-07 18:34:44');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png',NULL,'$2y$10$KrLyXYN/AmnugrPurJdzEuwvSCEMlRU1XQKNAIBj2WsAejRDmbhSq','Vq7B7KWgiiSNFSnSP5Lddmb28hGXSnn16q6JYWLicdf3fB0RytFnVSjeiIHj',NULL,'2020-03-07 18:34:44','2020-03-09 09:46:11',1),(3,2,'Акмарал','akmaral@supplygroup.asia','users/default.png',NULL,'$2y$10$FVbeQ/3jPWh4ywdnhgaMP.nlNCuKMksIcnLBaGFGmMh7s7PVzD7Rq',NULL,NULL,'2020-03-09 12:17:26','2020-03-09 12:17:26',1),(4,3,'Аскар','askar@supplygroup.asia','users/default.png',NULL,'$2y$10$iTIg/tjVoEyoswDtNh/O2.M/i1CJXGtW9ISuHHSe9YlOeMBWbwsVG',NULL,NULL,'2020-03-09 12:17:56','2020-03-09 12:17:56',1),(5,3,'Артём','artyom@spark.com','users/default.png',NULL,'$2y$10$FN0DbPEmteXV0/qpMU/IGukT6Q0vE4u31jhM0UMxZlUBuLISJQASe',NULL,NULL,'2020-03-09 12:36:17','2020-03-09 12:36:17',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-09 12:48:57
