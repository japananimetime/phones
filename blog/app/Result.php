<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Result
 *
 * @property int $id
 * @property int $file_id
 * @property string $name
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $row
 * @property int $phone_column
 * @property int|null $sheet_id
 * @property string|null $sheet_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result wherePhoneColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereRow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereSheetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereSheetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereValue($value)
 * @mixin \Eloquent
 */
class Result extends Model
{

}
