<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Phone
 *
 * @property int $id
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $ls
 * @property string|null $company
 * @property int $company_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereLs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Phone extends Model
{
    protected $fillable = ['phone', 'ls', 'company', 'company_id'];
}
