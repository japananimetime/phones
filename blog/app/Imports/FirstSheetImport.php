<?php

namespace App\Imports;

use App\Phone;
use App\Result;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class FirstSheetImport implements ToCollection, WithHeadingRow
{
    protected $file_id, $company_id, $column;

    public function __construct($file_id, $company_id, $column)
    {
        HeadingRowFormatter::default('none');
        $this->file_id = $file_id;
        $this->company_id = $company_id;
        $this->column = $column;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $phones = Phone::where('company_id', $this->company_id)->select('phone')->get()->pluck('phone')->toArray();
        $array = [];
        $row = 0;
        foreach ($collection as $item) {
            if (in_array($item[$this->column], $phones)) {
                array_push($array, [
                    'name' => 'Лицевой счёт',
                    'value' => Phone::where('phone', $item[$this->column])->first()->ls,
                    'row' => $row,
                    'file_id' => $this->file_id,
                    'phone_column' => 0,
                    'sheet_id' => 1,
                    'sheet_name' => 'Первый лист'
                ]);
                array_push($array, [
                    'name' => 'Наименование компании',
                    'value' => Phone::where('phone', $item[$this->column])->first()->company,
                    'row' => $row,
                    'file_id' => $this->file_id,
                    'phone_column' => 0,
                    'sheet_id' => 1,
                    'sheet_name' => 'Первый лист'
                ]);
                foreach ($item as $key => $value) {
                    if($key == $this->column){
                        array_push($array, [
                            'name' => $key,
                            'value' => $value,
                            'row' => $row,
                            'file_id' => $this->file_id,
                            'phone_column' => 1,
                            'sheet_id' => 1,
                    'sheet_name' => 'Первый лист'
                        ]);
                    }
                    else{
                        array_push($array, [
                            'name' => $key,
                            'value' => $value,
                            'row' => $row,
                            'file_id' => $this->file_id,
                            'phone_column' => 0,
                            'sheet_id' => 1,
                    'sheet_name' => 'Первый лист'
                        ]);
                    }
                }
                $row = $row + 1;
                Result::insert($array);
                $array=[];
            }
        }
    }
}
