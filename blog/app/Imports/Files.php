<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class Files implements WithMultipleSheets, SkipsUnknownSheets
{
    protected $file_id, $company_id, $column;

    public function __construct($file_id, $company_id, $column)
    {
        $this->file_id = $file_id;
        $this->company_id = $company_id;
        $this->column = $column;
    }

    public function sheets(): array
    {
        ini_set('max_execution_time', 0);
        return [
            new FirstSheetImport($this->file_id, $this->company_id, $this->column),
            new SecondSheetImport($this->file_id, $this->company_id, $this->column),
            new ThirdSheetImport($this->file_id, $this->company_id, $this->column),
            new ForthSheetImport($this->file_id, $this->company_id, $this->column),
            new FifthSheetImport($this->file_id, $this->company_id, $this->column),
            new SixSheetImport($this->file_id, $this->company_id, $this->column)
        ];
    }

    public function onUnknownSheet($sheetName)
    {
        // E.g. you can log that a sheet was not found.
        print_r("Sheet {$sheetName} was skipped");
    }
}
