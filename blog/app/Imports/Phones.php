<?php

namespace App\Imports;

use App\Phone;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class Phones implements ToCollection
{
    protected $company_id;

    function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        foreach ($collection->slice(1)->all() as $row) {
            Phone::create([
                'phone' => $row[0],
                'ls' => $row[1],
                'company' => $row[2],
                'company_id' => $this->company_id
            ]);
        }
    }
}
