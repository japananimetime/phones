<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
        return redirect('/admin');
    });

    Route::middleware('auth')->group(function () {

        Route::post('/upload', 'FileController@upload');

    });

    Route::group(['prefix' => 'admin'], function () {
        Voyager::routes();
    });

    Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
